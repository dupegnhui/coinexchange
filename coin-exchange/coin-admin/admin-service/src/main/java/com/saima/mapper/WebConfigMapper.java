package com.saima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.saima.domain.WebConfig;

public interface WebConfigMapper extends BaseMapper<WebConfig> {
}