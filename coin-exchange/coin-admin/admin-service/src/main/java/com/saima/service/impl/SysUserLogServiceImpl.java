package com.saima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.saima.domain.SysUserLog;
import com.saima.mapper.SysUserLogMapper;
import com.saima.service.SysUserLogService;
import org.springframework.stereotype.Service;
@Service
public class SysUserLogServiceImpl extends ServiceImpl<SysUserLogMapper, SysUserLog> implements SysUserLogService{

}
