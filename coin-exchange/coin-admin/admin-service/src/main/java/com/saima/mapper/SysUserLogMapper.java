package com.saima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.saima.domain.SysUserLog;

public interface SysUserLogMapper extends BaseMapper<SysUserLog> {
}