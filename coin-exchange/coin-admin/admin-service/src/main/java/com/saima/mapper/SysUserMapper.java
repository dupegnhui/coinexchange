package com.saima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.saima.domain.SysUser;

public interface SysUserMapper extends BaseMapper<SysUser> {
}