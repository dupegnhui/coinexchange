package com.saima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.saima.domain.SysRolePrivilegeUser;

public interface SysRolePrivilegeUserMapper extends BaseMapper<SysRolePrivilegeUser> {
}