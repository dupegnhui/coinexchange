package com.saima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.saima.domain.SysUserRole;

public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
}