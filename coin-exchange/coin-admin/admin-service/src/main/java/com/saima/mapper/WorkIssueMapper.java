package com.saima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.saima.domain.WorkIssue;

public interface WorkIssueMapper extends BaseMapper<WorkIssue> {
}