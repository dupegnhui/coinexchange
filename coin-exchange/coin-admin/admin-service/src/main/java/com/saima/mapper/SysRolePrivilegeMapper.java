package com.saima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.saima.domain.SysRolePrivilege;

public interface SysRolePrivilegeMapper extends BaseMapper<SysRolePrivilege> {
}