package com.saima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.saima.domain.Config;

public interface ConfigMapper extends BaseMapper<Config> {
}