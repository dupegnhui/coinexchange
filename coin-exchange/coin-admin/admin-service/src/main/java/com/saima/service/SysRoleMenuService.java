package com.saima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.saima.domain.SysRoleMenu;

public interface SysRoleMenuService extends IService<SysRoleMenu>{
}
