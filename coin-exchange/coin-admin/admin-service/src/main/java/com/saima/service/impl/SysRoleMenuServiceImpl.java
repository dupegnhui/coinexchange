package com.saima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.saima.domain.SysRoleMenu;
import com.saima.mapper.SysRoleMenuMapper;
import com.saima.service.SysRoleMenuService;
import org.springframework.stereotype.Service;
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService{

}
