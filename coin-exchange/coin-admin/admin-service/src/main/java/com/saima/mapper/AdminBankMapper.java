package com.saima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.saima.domain.AdminBank;

public interface AdminBankMapper extends BaseMapper<AdminBank> {
}