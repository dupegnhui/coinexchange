package com.saima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.saima.domain.Notice;

public interface NoticeMapper extends BaseMapper<Notice> {
}